//Version 1.0.0 Unity Map Build Tools released (Use to Unity 5.0 or higher)
//
//Don't forget add shader in buildsettings
//This tools only for TrackRacing Online 3500 or higher version
using System.Linq;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;
using print = UnityEngine.Debug;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using static UnityEngine.GraphicsBuffer;

public partial class BuildTools : EditorWindow
{
    public static void FixMaterials()
    {
        if (!RenderSettings.fog)
        {
            RenderSettings.fog = true;
            RenderSettings.fogDensity = 0.0002f;
        }
        string assetsPath = "Assets/!tracks/textures";
        Directory.CreateDirectory(assetsPath);
        foreach (Material m in Resources.FindObjectsOfTypeAll(typeof(Material)))
        {
            if (!m.shader.name.EndsWith("Diffuse")) continue;
            var mainTexture = m.mainTexture;
            var path = AssetDatabase.GetAssetPath(mainTexture);
            {
                Texture2D loadAssetAtPath = new[] { ".tif", ".png" }.Select(a => (Texture2D)AssetDatabase.LoadAssetAtPath(assetsPath + "/" + m.name + a, typeof(Texture2D))).FirstOrDefault(a => a != null);
                if (loadAssetAtPath != null)
                {
                    m.mainTexture = loadAssetAtPath;
                    EditorUtility.SetDirty(m);
                }
                else if (path.EndsWith(".dds"))
                {
                    print.Log("Creating png" + m.name);
                    var t = new Texture2D(mainTexture.width, mainTexture.height, TextureFormat.ARGB32, true);
                    t.SetPixels32(((Texture2D)mainTexture).GetPixels32());
                    var png = ((Texture2D)t).EncodeToPNG();
                    var newPath = assetsPath + "/" + m.name + ".png";
                    File.WriteAllBytes(newPath, png);
                    AssetDatabase.Refresh();
                    m.mainTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(newPath, typeof(Texture2D));
                }
            }
        }
    }
    [MenuItem("Building Tools/TrackRacing Online/Building Map for all device")]
    public static void BuildAll()
    {
        Build2(BuildTarget.StandaloneWindows64);
        Build2(BuildTarget.Android);
        Build2(BuildTarget.WebGL);
    }
    [MenuItem("Building Tools/TrackRacing Online/Building Map for Windows")]
    public static void BuildWin32()
    {
        Build2(BuildTarget.StandaloneWindows64);
    }
    [MenuItem("Building Tools/TrackRacing Online/Building Map for WebGL")]
    public static void BuildWebGL()
    {
        Build2(BuildTarget.WebGL);
    }
    [MenuItem("Building Tools/TrackRacing Online/Building Map for Android")]
    public static void BuildAndroid()
    {
        Build2(BuildTarget.Android);
    }
    public static void Build2(BuildTarget bt)
    {
#if UNITY_2023_1_OR_NEWER
        var scene = EditorApplication.currentScene;
        var d = SceneManager.GetActiveScene();
        EditorSceneManager.SaveScene(d);
        if (Path.GetFileName(scene) != Path.GetFileName(scene).ToLower())
        {
            print.Log("Rename");
            File.Move(scene, scene.ToLower());
            scene = scene.ToLower();
            AssetDatabase.Refresh();
        }
        FixMaterials();
        EditorSceneManager.SaveScene(d);
        var assetBundleName = d.name + (bt == BuildTarget.StandaloneWindows64 ? ".unity3dwindows" : bt == BuildTarget.WebGL ? ".unity3dwebgl" : ".unity3dandroid");
        var build = new AssetBundleBuild[]
        {
            new AssetBundleBuild
            {
                assetBundleName = assetBundleName,
                assetNames = new string[] { d.path }
            }
        };
        var bundlesDir = "maps";
        Directory.CreateDirectory(bundlesDir);
        BuildAssetBundlesParameters buildInput = new()
        {
            outputPath = bundlesDir,
            bundleDefinitions = build
        };
        AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(buildInput);
        if (manifest != null)
        {
            var outputFiles = Directory.EnumerateFiles(bundlesDir, "*", SearchOption.TopDirectoryOnly);
            Debug.Log("Output of the build:\n\t" + string.Join("\n\t", outputFiles));
        }

#else
        var scene = EditorApplication.currentScene;
        var d = SceneManager.GetActiveScene();
        EditorSceneManager.SaveScene(d);
        if (Path.GetFileName(scene) != Path.GetFileName(scene).ToLower())
        {
            print.Log("Rename");
            File.Move(scene, scene.ToLower());
            scene = scene.ToLower();
            AssetDatabase.Refresh();
        }
        FixMaterials();
        EditorApplication.SaveScene();
        var f = Path.GetFileNameWithoutExtension(scene) + ".unity3d" +
            (bt == BuildTarget.iOS ? "ios" : bt == BuildTarget.Android ? "android" : bt == BuildTarget.StandaloneWindows64 ? "windows" : bt == BuildTarget.StandaloneLinux64 ? "linux" : bt == BuildTarget.StandaloneOSX ? "macox" : "webgl");
        File.Delete(f);
        Directory.CreateDirectory("maps");
        BuildPipeline.BuildStreamedSceneAssetBundle(new[] { scene }, "maps/" + f, bt);
#endif
    }
}
public class EditorPopup : EditorWindow
{
    public static string te;
    public static void ShowPopup(string text)
    {
        te = text;
        var window = EditorWindow.GetWindow<EditorPopup>("Error");
        window.position = new Rect(Screen.width / 2f, Screen.height / 2f, 350, 150);
        window.Show();
    }
    public void OnGUI()
    {
        GUILayout.Label(te);
    }
}
